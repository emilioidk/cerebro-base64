# Cerebro Base64 converter plugin

Simple plugin for converting from and to base 64.

![](assets/usage.gif)

## Usage

Start your input with `base64 ` and then add the string you want to encode or decode. The plugin will always
encode the inputted term but will only add a decoded item if the string is padded to 4 characters. The
keyboard shortcut `CONTROL+C` or selecting the item on the UI will copy the UUID to the clipboard.
