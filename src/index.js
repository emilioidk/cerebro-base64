import iconEncode from '../assets/encode.png'
import iconDecode from '../assets/decode.png'

export const name = 'base64'
export const keyword = 'base64'

function decode(string) {
  return Buffer.from(string, 'base64').toString('utf-8')
}

function encode(string) {
  return Buffer.from(string, 'utf-8').toString('base64')
}

export const fn = ({ actions, term, display }) => {
  const results = []

  if(!term.startsWith('base64 '))
    return;

  const str = term.substr(7)

  if (str.length === 0)
    return;

  // Always encode
  const encoded = encode(str)
  results.push({
    title: `Base 64 encoded: "${encoded}"`,
    subtitle: 'CTRL+C or ENTER copies to clipboard',
    icon: iconEncode,
    clipboard: encoded,
    onSelect: () => actions.copyToClipboard(encoded)
  })

  if (str.length % 4 === 0) {
    // Decode if padded to 4
    const decoded = decode(str)
    results.push({
      title: `Base 64 decoded: "${decoded}"`,
      subtitle: 'CTRL+C or ENTER copies to clipboard',
      icon: iconDecode,
      clipboard: decoded,
      onSelect: () => actions.copyToClipboard(decoded)
    })
  }

  // Put your plugin code here
  display(results)
}
